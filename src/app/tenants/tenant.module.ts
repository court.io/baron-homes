import { TenantEditComponent } from './tenant-edit/tenant-edit.component';
import { TenantRouting } from './tenant.routing';
import { TenantSearchComponent } from './tenant-search/tenant-search.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TenantProfileComponent } from './tenant-profile/tenant-profile.component';

@NgModule({
  imports: [
    CommonModule,
    TenantRouting
  ],
  declarations: [
    TenantProfileComponent,
    TenantSearchComponent,
    TenantEditComponent
  ]
})
export class TenantModule { }
